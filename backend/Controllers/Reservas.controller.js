const _ = require('underscore');
const Reservas = require('../Models/Reservas.model')


let CreateReserv = (req, res) => {
    let body = _.pick(req.body, [
        'date',
        'hours'
    ]);

    let newReserv = new Reservas(body);

    Reservas.countDocuments({ date: body.date }, (err, cont) => {
        if (err) res.status(500)
            .json({
                ok: false,
                err
            });
        if (cont >= 2) res.status(300)
            .json({
                ok: false,
                msg: `Nuestra agenda para este dia ya se encuentra ocupada`
            });
        newReserv.save((err, reserva) => {
            if (err) res.status(500)
                .json({
                    ok: false,
                    err
                });
            res.json({
                ok: true,
                reserva
            })
        })
    })
}

let GetReservs = (req, res) => {
    Reservas.find({}, (err, reservas) => {
        if (err) res.status(500).json({ ok: false, err })
        res.json({
            ok: true,
            reservas
        })
    })

}

let GetReserv = (req, res) => {
    let id = req.params.id;
    Reservas.findById(id, (err, reserva) => {
        if (err) res.status(500).json({ ok: false, err })
        res.json({
            ok: true,
            reserva
        })
    })
}

let UpdateReserv = (req, res) => {

}

let DeleteReserv = (req, res) => {
    let id = req.params.id;

    Reservas.findByIdAndUpdate(id, { deleted_at: Date.now() }, (err, reserva) => {
        if (err) res.status(500).json({ ok: false, err })
        res.json({
            ok: true,
            reserva
        })
    })
}


module.exports = {
    CreateReserv,
    GetReservs,
    GetReserv,
    UpdateReserv,
    DeleteReserv
}