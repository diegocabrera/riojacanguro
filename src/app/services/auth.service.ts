import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { LoginModel } from "../models/loginModel";
import { SignupModel } from "../models/signup.model";

@Injectable({
	providedIn: "root"
})
export class AuthService {
	private url = `http://localhost:${3000}/api/`;
	//private apiKey = "window.location.origin";
	userToken: string;

	constructor(private http: HttpClient) {
		console.log(window.location.origin);
	}

	login(user: LoginModel) {
		Number(user.user);
		let userLogin = {
			...user
		};
		return this.http.post(`${this.url}login`, userLogin).pipe(
			map(resp => {
				this.tokeStorage(resp["token"]);
				return resp;
			})
		);
	}

	signUp(user: SignupModel) {
		let newUser = {
			...user
		};

		return this.http.post(`${this.url}signup`, newUser).pipe(
			map(resp => {
				this.tokeStorage(resp["token"]);
				return resp;
			})
		);
	}

	private tokeStorage(token: string) {
		this.userToken = token;
		localStorage.setItem("token", token);
	}

	readToken() {
		if (localStorage.getItem("token")) {
			this.userToken = localStorage.getItem("token");
		} else {
			this.userToken = "";
		}
		return this.userToken;
	}
}
